import React from "react";
import {NotificationContainer, NotificationManager} from 'react-notifications';
import Main from "./components/main";
import 'react-notifications/lib/notifications.css';
class App extends React.Component{

  constructor(props) {
        super(props);
        this.state = {inputList: [ <Main /> ]};
        this.onAddBtnClick = this.onAddBtnClick.bind(this);
        this.onRemoveBtnClick = this.onRemoveBtnClick.bind(this);

    }

    onAddBtnClick(event) {
        const inputList = this.state.inputList;
        NotificationManager.success('График добавлен!');
        this.setState({
            inputList: inputList.concat(<Main key={inputList.length} />)
        });
    }

    onRemoveBtnClick(event) {
        const inputList = this.state.inputList;

        if (inputList.length !== 0){
          inputList.splice(-1,1);
          NotificationManager.error('График удален!');
        }
        else{
          console.log("try remove empty");
        }
        this.setState({
            inputList: inputList
        });
    }

    render(){

      return(
        <div class="main" >
          <h1 class="maintext">MARAXIM.&shy;INVEST.&shy;OGR</h1>
          <h2 class="maintext">Простой и удобный сервис для инвестиций</h2>
          <p> Введите код ценной бумаги, выберите вариант отображения и получите информацию! </p>

          {this.state.inputList.map(function(input, index) {
                   return input;
          })}

          <p> Вы также можете добавить или удалить дополнительные графики нажимая на + и - !</p>
          <div class="buttonContainer">

            <button onClick={this.onAddBtnClick} class="plusminus">
                 <img  width="50" height="50" src="plus.png" alt="Добавить график"  />
            </button>
            
            <button onClick={this.onRemoveBtnClick} class="plusminus">
                 <img width="50" height="50" src="minus.png" alt="Удалить график"  />
            </button>
          </div>
          <NotificationContainer/>

          <p class="maintext plain">Созданно при поддержке TFS-2018</p>


        </div>
      );
    }
}

export default App;
