import React from "react";
import { observer } from "mobx-react"
import { AreaChart, Area, XAxis, YAxis, Tooltip, CartesianGrid, Brush, Legend,
  ReferenceArea, ReferenceLine, ReferenceDot, ResponsiveContainer,
  LabelList, Label } from 'recharts';
  import Toggle from "react-toggle-component"
  import "react-toggle-component/styles.css"

class  Graph extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
        open:"1",
        high: "1",
        close: "1",
        low:"1",
    }
  }

  makeRenderData = (dataForGraph) => {
    let newData=[];
    for (let i=0;i<dataForGraph.length;i++){
      newData.push({});
      newData[i]['name']=dataForGraph[i]['name'];
      if (this.state.open) {
        newData[i]['op']=dataForGraph[i]['op'];
      }
      if (this.state.high) {
        newData[i]['uv']=dataForGraph[i]['uv'];
      }
      if (this.state.close) {
        newData[i]['cl']=dataForGraph[i]['cl'];
      }
      if (this.state.low) {
        newData[i]['pv']=dataForGraph[i]['pv'];
      }
    }
    //console.log(newData);
    return newData;
  }

  render(){
      let  dataForGraph=this.props.dataForGraph;
      let dataForRender=this.makeRenderData(dataForGraph);

        return(
          <div class='graph'>
            <div> График котировок ценной бумаги с кодом {this.props.symbol} </div>
            <div class='toggleContainer'>
                <Toggle
                  id={Math.random()+''}
                  class='toggle'
                  defaultChecked={this.state.open}
                  onChange= { e =>
                      { if (e.target) this.setState({open:e.target.checked})}
                   }
                  label="Цена открытия" />

                <Toggle
                  id={Math.random()+''}
                  class="toggle"
                  defaultChecked={this.state.high}
                  onChange= { e =>
                      { if (e.target) this.setState({high:e.target.checked})}
                   }
                  label="Верхняя цена" />
                <Toggle
                  id={Math.random()+''}
                  class="toggle"
                  defaultChecked={this.state.low}
                  onChange= { e =>
                      { if (e.target) this.setState({low:e.target.checked})}
                   }
                  label="Нижняя цена" />
                <Toggle
                  id={Math.random()+''}
                  class="toggle"
                  defaultChecked={this.state.close}
                  onChange= { e =>
                      { if (e.target) this.setState({close:e.target.checked})}
                   }
                  label="Цена закрытия" />
            </div>
          <ResponsiveContainer height={250}>
              <AreaChart data={dataForRender}
                margin={{ top: 30, right: 30, left: 0, bottom: 10 }}>
                <defs>
                  <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8}/>
                    <stop offset="95%" stopColor="#82ca9d" stopOpacity={0}/>
                  </linearGradient>
                  <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8}/>
                    <stop offset="95%" stopColor="#8884d8" stopOpacity={0}/>
                  </linearGradient>
                  <linearGradient id="colorOpen" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="5%" stopColor="#EE82EE" stopOpacity={0.8}/>
                    <stop offset="95%" stopColor="#EE82EE" stopOpacity={0}/>
                  </linearGradient>
                  <linearGradient id="colorClose" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="5%" stopColor="#2F4F4F" stopOpacity={0.8}/>
                    <stop offset="95%" stopColor="#2F4F4F" stopOpacity={0}/>
                  </linearGradient>
                </defs>
                <XAxis dataKey="name"  />
                <YAxis />
                <CartesianGrid strokeDasharray="3 3" />
                <Tooltip />
                <Area name="Верхняя цена" type="monotone" dataKey="uv"  stroke="#82ca9d" fillOpacity={0.5} fill="url(#colorUv)" />
                <Area name="Нижняя цена" type="monotone" dataKey="pv"  stroke="#8884d8" fillOpacity={0.5} fill="url(#colorPv)" />
                <Area name="Цена открытия" type="monotone" dataKey="op"  stroke="#EE82EE" fillOpacity={0.5} fill="url(#colorOpen)" />
                <Area name="Цена закрытия" type="monotone" dataKey="cl"  stroke="#2F4F4F" fillOpacity={0.5} fill="url(#colorClose)" />

              </AreaChart>
           </ResponsiveContainer>
          </div>
        );
      }
  }

  export default Graph;
