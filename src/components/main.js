import React from "react";

import Graph from "./graph";
import InputPanel from "./inputPanel";

const API_KEY = "2HHIGRY27JRKN13M";
// const API_KEY = "demo";

class Main extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          symbol:"EXPM (пример)",
          queryType: "0",
          dataForGraph: [
            {
              "name":"Вчера",
              "op": "108.1000",
               "uv": "200",
               "pv": "50",
               "cl": "104.9700",
               "vol": "39451205"},
           {
             "name":"Сегодня",
             "op": "180",
              "uv": "250",
              "pv": "70",
              "cl": "140.9700",
              "vol": "39451205"
            },
            {
              "name":"Завтра",
              "op": "108.1000",
               "uv": "200",
               "pv": "50",
               "cl": "104.9700",
               "vol": "39451205"
             }
          ],
      }
    }

    getInfoFromAPI = (e) => {
        e.preventDefault();
        const symbol = e.target.elements.symbol.value;
        const queryType =  e.target.elements.queryType.value;

        this.getDataFromAPI(symbol,queryType);
        this.setState({symbol:symbol,queryType:queryType});
    }

    getDataFromAPI = async (symbol, queryType) =>  {
        let api_url;
        console.log('start get');
        switch (queryType) {
            case "0":
                api_url= await fetch(
                  `https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=${symbol}&interval=5min&apikey=${API_KEY}`
                );
                break;
            case "1":
                api_url= await fetch(
                  `https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=${symbol}&apikey=${API_KEY}`
                );
                break;
            case "2":
                api_url= await fetch(
                  `https://www.alphavantage.co/query?function=TIME_SERIES_WEEKLY&symbol=${symbol}&apikey=${API_KEY}`
                );
                break;
            default:
                api_url= await fetch(
                  `https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY&symbol=${symbol}&apikey=${API_KEY}`
                );
        }
        let tmpData =  await api_url.json();
        this.makeValidData(tmpData);
    }

    makeValidData = (jsonData) =>{
      if (!jsonData) return;
      let keys = [];

      for(var k in jsonData) keys.push(k);

      jsonData=jsonData[keys[1]];

      let newData=[];
      for (let curRowData in jsonData ){ // для каждой даты
        let newRow={};

        newRow['name']=curRowData;
        newRow['op']=jsonData[curRowData]["1. open"];
        newRow['uv']=jsonData[curRowData]["2. high"];
        newRow['pv']=jsonData[curRowData]["3. low"];
        newRow['cl']=jsonData[curRowData]["4. close"];
        newData.push(newRow);
      }
      this.setState({ dataForGraph:newData.reverse() });
    }

  render() {
    return(
      <div class="graphContainer">
        <Graph name="graph" dataForGraph={this.state.dataForGraph} symbol={this.state.symbol}/>
        <InputPanel  getMethod={this.getInfoFromAPI} />
      </div>
      )
  }
}
export default Main;
