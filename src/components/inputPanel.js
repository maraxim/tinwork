import React from "react";
import Toggle from "react-toggle-component"
import "react-toggle-component/styles.css"

class InputPanel extends React.Component {
  render() {
    return(
      <form class="inputPanel" onSubmit={this.props.getMethod}>

          <select class='maintext' name="queryType">
              <option selected value="0">Сегодня</option>
              <option value="1">По дням</option>
              <option value="2">По неделям</option>
              <option value="3">По месяцам</option>

          </select>
          <p>
            <input class='maintext' type="text" name="symbol" list="symbols" placegolder="КОД"/>
            <datalist id="symbols">
              <option>AAPL</option>
              <option>USD</option>
              <option>MSFT</option>
              <option>GOOG</option>
              <option>FB</option>
              <option>INTC</option>
              <option>TSLA</option>
              <option>PYPL</option>
              <option>KO</option>
              <option>MCD</option>
            </datalist>
          </p>
          <p>
          <button class='maintext'> Получить информацию </button>
          </p>
      </form>
    );
  }
}

export default InputPanel;
